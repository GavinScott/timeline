from __future__ import print_function
import httplib2
import os
import os.path

from apiclient import discovery
import oauth2client
from oauth2client import client
from oauth2client import tools

import datetime
import dateutil.parser
import sys
import math
import time

import gen_autorun
import version
import select

from datetime import tzinfo, timedelta

ZERO = timedelta(0)

DONE = 'timeline=done'

NUM_EVENTS_PER_CAL = 15

UPDATE = 'u'

class bcolors:
    DONE = '\033[92m'
    NOT_DONE = '\033[94m'
    ENDC = '\033[0m'
    YELLOW = '\033[93m'
    ERROR = '\033[91m'
    BOLD = '\033[1m'
    HEADER = '\033[95m'

class UTC(tzinfo):
  def utcoffset(self, dt):
    return ZERO
  def tzname(self, dt):
    return "UTC"
  def dst(self, dt):
    return ZERO

service = None

# If modifying these scopes, delete your previously saved credentials
# at ~/.credentials/calendar-python-quickstart.json
SCOPES = 'https://www.googleapis.com/auth/calendar'
CLIENT_SECRET_FILE = '.client_secret.json'
APPLICATION_NAME = 'Timeline'
CACHE_FILE = '.selected-calendars'

SETUP_FILE = './.setup.sh'
CHECK_SETUP = '.verify'

REFRESH_DELAY = 10
MIN_DELAY = 10

# flags
CLEAR_FLAG = ['-c', '-clear']
HELP_FLAG = ['-h', '-help']
REFRESH_FLAG = ['-r', '-refresh']
AUTO_FLAG = ['-a', '-auto']
HELP_TEXT = """COMMANDS:
    \t-c, -clear   : deletes cache
    \t-h, -help    : lists available commands
    \t-r, -refresh : set the refresh rate
    \t-a, -auto    : toggle autorun on login
    """
    
class Cal:
    def __init__(self, name, ID):
         self.name = name
         self.ID = ID

class MyEvent:
     def __init__(self, time, event, cal, descr, ID):
         self.time = time
         self.event = event
         self.cal = cal
         self.name = event['summary']
         self.descr = descr.lower().strip()
         self.ID = ID

def get_credentials():
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    home_dir = os.path.expanduser('~')
    credential_dir = os.path.join(home_dir, '.credentials')
    if not os.path.exists(credential_dir):
        os.makedirs(credential_dir)
    credential_path = os.path.join(credential_dir,
                                   'calendar-python-quickstart.json')

    store = oauth2client.file.Storage(credential_path)
    credentials = store.get()
    if not credentials or credentials.invalid:
        flow = client.flow_from_clientsecrets(CLIENT_SECRET_FILE, SCOPES)
        flow.user_agent = APPLICATION_NAME
        credentials = tools.run_flow(flow, store, None)
        print('Storing credentials to ' + credential_path)
    return credentials

def getEventStart(event):
    start = event['start'].get('dateTime', event['start'].get('date'))
    return dateutil.parser.parse(start)

def getEvents(selectedCalendars):
    global service
    now = datetime.datetime.utcnow().isoformat() + 'Z' # 'Z' indicates UTC time
    
    allEvents = []
    for cal in selectedCalendars:
        eventsResult = service.events().list(
            calendarId=cal.ID, timeMin=now, maxResults=NUM_EVENTS_PER_CAL,
            singleEvents=True, orderBy='startTime').execute()
        events = eventsResult.get('items', [])

        if not events:
            print('\tNo upcoming events found.')
        for event in events:
            descr = ''
            if 'description' in event:
                descr = event['description']
            allEvents.append(MyEvent(getEventStart(event), event, cal, descr, event['id']))
    allEvents.sort(key=lambda e: e.time)
    return allEvents

def getFullCalendarList():
    cals = {}
    global service
    page_token = None
    while True:
      calendar_list = service.calendarList().list(pageToken=page_token).execute()
      for entry in calendar_list['items']:
        cals[entry['summary']] = Cal(entry['summary'], entry['id'])        
      page_token = calendar_list.get('nextPageToken')
      if not page_token:
        break
    return cals

def getCachedCalendars():
    if os.path.isfile(CACHE_FILE):
        with open(CACHE_FILE, 'r') as f:
            cals = [x.strip() for x in f.readlines() if len(x.strip()) > 0]
            if len(cals) > 0:
                return [Cal(x.split('|')[0], x.split('|')[1]) for x in cals]
            else:
                return None
    else:
        return None
        
def cacheCalendars(cals):
    with open(CACHE_FILE, 'w+') as f:
        for cal in cals:
            f.write(cal.name + '|' + cal.ID + '\n')

def auth():
    global service
    # authenticate
    credentials = get_credentials()
    http = credentials.authorize(httplib2.Http())
    service = discovery.build('calendar', 'v3', http=http)

def getSelectedCalendars():
    # get selected calendars
    cals = getCachedCalendars()
    if cals == None:
        # select calendars
        cals = getFullCalendarList()
        for i in range(0, len(cals)):
            print(i, cals.keys()[i])
        selected_cals = None
        while selected_cals == None:
            inp = raw_input("\nSelect the #'s of the calendar's you want: ")
            try:
                selected_cals = [cals[cals.keys()[int(x)]] for x in inp.split()]
            except:
                print('Please enter a valid list of integers')
        # cache selection
        cals = selected_cals
        cacheCalendars(cals)
    return cals

def handleFlags():
    if len(sys.argv) > 1:
        flag = sys.argv[1]
        # delete cache
        if flag in CLEAR_FLAG and os.path.isfile(CACHE_FILE):
            os.remove(CACHE_FILE)
        # show help message
        elif flag in HELP_FLAG:
            print(HELP_TEXT)
            exit(0)
        elif flag in REFRESH_FLAG:
            if len(sys.argv) < 3:
                print('Must specify a number of seconds between refreshes')
                print('Example: "python timeline.py -r 10"')
                exit(-1)
            try:
                REFRESH_RATE = int(sys.argv[2])
                if REFRESH_RATE < MIN_DELAY:
                    exit(0)
            except:
                print('Refresh rate must be a valid integer of at least', MIN_DELAY, 'seconds')
                exit(-1)
        elif flag in AUTO_FLAG:
            gen_autorun.toggle()

def pluralize(num, units):
    num = int(round(num))
    if num > 1:
        units += 's'
    return str(num) + ' ' + units

def getPreciseTimeString(w, d, h, m, s):
    strs = []
    if w > 0:
        strs.append(pluralize(w, 'week'))
    # only show days if less than two weeks left
    if d > 0 and w <= 1:
        strs.append(pluralize(d, 'day'))
    # only show hours if less than a day left
    if h > 0 and d <= 1 and w == 0:
        strs.append(pluralize(h, 'hour'))
    # only show minutes if less than 6 hours left
    if m > 0 and d == 0 and w == 0:
        strs.append(pluralize(m, 'min'))
    # only show seconds if less than a minute left
    if s > 0 and h == 0 and d == 0 and w == 0:
        strs.append(pluralize(s, 'sec'))
    return ', '.join(strs)

def getPreciseTime(event):
    now = datetime.datetime.now(UTC())
    s = int(math.floor((event.time - now).total_seconds()))
    h = 0
    m = 0
    d = 0
    w = 0
    if s >= 60:
        m = s / 60
        s = s - 60 * m
    if m >= 60:
        h = m / 60
        m = m - 60 * h
    if h >= 24:
        d = h / 24
        h = h - 24 * d
    if d >= 7:
        w = d / 7
        d = d - 7 * w
    return getPreciseTimeString(w, d, h, m, s)
    
def clear_screen():
    print('\033c')

def handle_install():
    if not os.path.isfile(CHECK_SETUP):
        import subprocess
        print('Running installation script...')
        time.sleep(3)
        ret = subprocess.call(SETUP_FILE, shell=True)
        if ret == 0:
            with open(CHECK_SETUP, 'w+') as f:
                f.write('.')
        else:
            print('\n\n*** Error during required library installation ***')
            exit(-1)

def toggleEventFinish(events):
    for event in events:
        cal = event.cal.ID
        
        descr = event.descr
        if DONE in descr:
            print('Setting "' + event.name + '" as NOT finished')
            descr = descr.replace(DONE, '')
        else:
            print('Setting', event.name, 'as finished!')
            descr += DONE
        
        event = service.events().get(calendarId=event.cal.ID, eventId=event.event['id']).execute()
        event['description'] = descr
        updated_event = service.events().update(calendarId=cal, eventId=event['id'], body=event).execute()

def printColored(text, color):
    print(color + text + bcolors.ENDC)

def main():
    auth()
    handleFlags()
    clear_screen()
    # get calendars
    cals = getSelectedCalendars()
    
    # start continuous loop
    while True:
        # get events from selected calendars
        events = getEvents(cals)
        #clear_screen()
        # find dimensions
        maxLen = 0
        for e in events:
            maxLen = max(maxLen, len(e.name))
        # build divider
        div = ''
        for x in range(0, maxLen + 32):
            div += '-'
        # print events
        printColored(div, bcolors.HEADER)
        f = '%' + str(maxLen) + 's %21s'
        for i in range(0, len(events)):
            e = events[i]
            color = bcolors.NOT_DONE
            if DONE in e.descr:
                color = bcolors.DONE
            print(' ', bcolors.HEADER + str(i + 1), '\t', color + f % (e.name, getPreciseTime(e)) + bcolors.ENDC)
        printColored(div, bcolors.HEADER)
        if version.check_for_update():
            printColored('Enter "' + UPDATE + '" to update to version ' + \
                str(version.get_global_version())  + '!', bcolors.DONE)
        else:
            print()
        printColored('Enter a number of an event to toggle its status (expect a delay)', bcolors.YELLOW)
        printColored('(Press Ctrl+C to quit)', bcolors.YELLOW)
        while sys.stdin in select.select([sys.stdin], [], [], 0)[0]:
          line = sys.stdin.readline()
          if line:
            try:
                if line.strip() == UPDATE:
                    version.update()
                else:
                    toggleEventFinish([events[int(x.strip()) - 1] for x in line.split()])
            except:
                printColored( 'Please enter a valid event number (left hand column)', bcolors.ERROR)
          else: # an empty line means stdin has been closed
            print('eof')
            exit(0)
        # refresh
        time.sleep(REFRESH_DELAY)

if __name__ == '__main__':
    main()
