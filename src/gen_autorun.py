import os

NEW_TERMINAL_SCRIPT = '.new_term.sh'
AUTORUN_SCRIPT = 'autorun_timeline_script.sh'
AUTORUN_DIR = '/etc/profile.d/'

SCRIPT_CONTENT = 'x-terminal-emulator -e %s%s python %stimeline.py'

# toggles autorun
def toggle():
    print('Feature currently disabled')
    exit(0)
    try:
        if os.path.isfile(AUTORUN_DIR + AUTORUN_SCRIPT):
            os.remove(AUTORUN_DIR + AUTORUN_SCRIPT)
            print('Autorun disabled')
        else:
            generateAutoRun()
    except:
        print('Please run the script under sudo to generate the auto-run script')
        exit(-1)
    exit(0)

# autorun generator
def generateAutoRun():
    p = '/'.join(os.path.realpath(__file__).split('/')[:-1]) + '/'
    with open(AUTORUN_DIR + AUTORUN_SCRIPT, 'w+') as f:
        f.write(SCRIPT_CONTENT % (p, NEW_TERMINAL_SCRIPT, p))
    print('Autorun enabled')
    
