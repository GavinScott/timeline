import requests
import re
import wget
import zipfile
import os
import shutil

VERSION_FILE = '.version'
VERSION_URL = 'http://www.gavinoscott.com/timeline.html'
PATTERN = 'Current Version: ([0-9]+\.[0-9]+)'
SRC_URL = 'http://www.gavinoscott.com/uploads/5/7/4/0/57406539/timeline.zip'
TEMP = "temp/"

def get_local_version():
    with open(VERSION_FILE, 'r') as f:
        return float(f.readline().strip())

def get_global_version():
    try:
        r = requests.get(VERSION_URL)
        return float(re.findall(PATTERN, r.text)[0])
    except:
        # error retrieving global version
        return None

def check_for_update():
    return get_local_version() != get_global_version()

def move_files_up(directory, subdir=None):
    files = os.listdir(directory)
    for f in files:
         if os.path.isdir(directory + f + '/'):
            if os.path.exists(f):
                shutil.rmtree(f)
            shutil.copytree(directory + f + '/', './' + f + '/')
         else:
            shutil.copy(directory + f, '.')
    print(directory, ':', files)

def update():
    if check_for_update():
        print('Beginning Update:')
        if os.path.exists(TEMP):
            shutil.rmtree(TEMP)
        os.mkdir(TEMP)
        filename = wget.download(SRC_URL)
        with zipfile.ZipFile(filename,"r") as zip_ref:
            zip_ref.extractall(TEMP)
        os.remove(filename)
        move_files_up(TEMP)
        shutil.rmtree(TEMP)
        print('Update complete. Please restart timeline.')
        exit(1)
    else:
        print('No update needed.')

if __name__ == '__main__':
    update()
